package com.brom.epam.task10.controller;

import com.brom.epam.task10.model.CustomReadWriteLock;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Controller {
  private Logger logger = LogManager.getLogger(Controller.class.getName());

  public void executeLockExample() {
    logger.info("-----------one lock----------------");
    criticalSectionWithOneLock();
    logger.info("-----------few locks----------------");
    criticalSectionWithFewLocks();
  }

  private void criticalSectionWithOneLock() {
    ReentrantLock lock = new ReentrantLock();
    AtomicInteger a = new AtomicInteger();
    Thread thread1 = new Thread(() -> {
      lock.lock();
      a.addAndGet(10);
      try {
        Thread.sleep(2000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      lock.unlock();
    });
    Thread thread2 = new Thread(() -> {
      lock.lock();
      a.addAndGet(10);
      try {
        Thread.sleep(2000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      lock.unlock();
    });
    Thread thread3 = new Thread(() -> {
      lock.lock();
      a.addAndGet(10);
      try {
        Thread.sleep(2000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      lock.unlock();
    });
    thread1.start();
    thread2.start();
    thread3.start();
    try {
      Thread.sleep(1000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    logger.info(a);
  }

  private void criticalSectionWithFewLocks() {
    ReentrantLock lock1 = new ReentrantLock();
    ReentrantLock lock2 = new ReentrantLock();
    ReentrantLock lock3 = new ReentrantLock();
    AtomicInteger a = new AtomicInteger();
    Thread thread1 = new Thread(() -> {
      lock1.lock();
      a.addAndGet(10);
      try {
        Thread.sleep(2000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      lock1.unlock();
    });
    Thread thread2 = new Thread(() -> {
      lock2.lock();
      a.addAndGet(10);
      try {
        Thread.sleep(2000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      lock2.unlock();
    });
    Thread thread3 = new Thread(() -> {
      lock3.lock();
      a.addAndGet(10);
      try {
        Thread.sleep(2000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      lock3.unlock();
    });
    thread1.start();
    thread2.start();
    thread3.start();
    try {
      Thread.sleep(1000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    logger.info(a);
  }

  public void showPipedThread() {
    BlockingQueue<Character> symbolsQueue = new LinkedBlockingQueue<>();
    String text = "Some text to transfer between threads\n";
    Thread writerThread = getWriterThread(symbolsQueue, text);
    Thread readerThread = getReaderThread(symbolsQueue);
    writerThread.start();
    readerThread.start();
    try {
      writerThread.join();
      readerThread.join();
    } catch (InterruptedException e) {
      logger.error(e);
    }
  }

  private Thread getReaderThread(BlockingQueue<Character> queue) {
    return new Thread(() -> {
      while (!queue.isEmpty()) {
        logger.info(queue.poll());
      }
    });
  }

  private Thread getWriterThread(BlockingQueue<Character> queue, String text) {
    return new Thread(() -> {
      for (char c : text.toCharArray()) {
        queue.add(c);
      }
    });
  }

  public void testCustomLock() {
    CustomReadWriteLock.WriteLock writeLock = new CustomReadWriteLock().writeLock();
    CustomReadWriteLock.ReadLock readLock = new CustomReadWriteLock().readLock();
    AtomicInteger a = new AtomicInteger();
    Thread thread1 = new Thread(() -> {
      writeLock.lock();
      a.addAndGet(10);
      try {
        Thread.sleep(2000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      writeLock.unlock();
    });
    Thread thread2 = new Thread(() -> {
      readLock.lock();
      logger.info("Thread 2 a = " + a);
      try {
        Thread.sleep(2000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      readLock.unlock();
    });
    Thread thread3 = new Thread(() -> {
      readLock.lock();
      logger.info("Thread 3 a = " + a);
      try {
        Thread.sleep(2000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      readLock.unlock();
    });
    thread1.start();
    thread2.start();
    thread3.start();
    try {
      Thread.sleep(1000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

}
