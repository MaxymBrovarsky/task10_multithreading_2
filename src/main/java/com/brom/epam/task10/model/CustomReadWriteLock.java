package com.brom.epam.task10.model;

import java.util.concurrent.atomic.AtomicInteger;

public class CustomReadWriteLock {
  private final CustomReadWriteLock.ReadLock readerLock;
  private final CustomReadWriteLock.WriteLock writerLock;
  private AtomicInteger readLockCount = new AtomicInteger(0);
  private AtomicInteger writeLockCount = new AtomicInteger(0);

  public CustomReadWriteLock.WriteLock writeLock() {
    return writerLock;
  }

  public CustomReadWriteLock.ReadLock readLock() {
    return readerLock;
  }

  public CustomReadWriteLock() {
    readerLock = new ReadLock();
    writerLock = new WriteLock();
  }

  public class ReadLock {

    public void lock() {
      if (writeLockCount.get() == 0) {
        readLockCount.incrementAndGet();
      } else {
        while (writeLockCount.get() != 0) {
          try {
            Thread.currentThread().sleep(1000);
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
        }
      }
    }

    public void unlock() {
      readLockCount.decrementAndGet();
    }

  }

  public class WriteLock {

    public void lock() {
      if (writeLockCount.get() == 0 && readLockCount.get() == 0) {
        writeLockCount.incrementAndGet();
      } else {
        while (writeLockCount.get() != 0 && readLockCount.get() != 0) {
          try {
            Thread.currentThread().sleep(1000);
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
        }
      }
    }

    public void unlock() {
      writeLockCount.decrementAndGet();
    }

  }
}
