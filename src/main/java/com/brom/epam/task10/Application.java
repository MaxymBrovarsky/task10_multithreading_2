package com.brom.epam.task10;

import com.brom.epam.task10.view.View;

public class Application {
  public static void main(String[] args) {
    new View().show();
  }
}
