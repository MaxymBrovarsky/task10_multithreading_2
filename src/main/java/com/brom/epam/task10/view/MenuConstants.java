package com.brom.epam.task10.view;

public class MenuConstants {
  public static final String MONITOR_KEY = "1";
  public static final String PIPE_KEY = "2";
  public static final String CUSTOM_LOCK_KEY = "3";
  public static final String QUIT_KEY = "4";
  public static final String MONITOR_TEXT = "1. Monitor threads";
  public static final String PIPE_TEXT = "2. Piped threads";
  public static final String CUSTOM_LOCK_TEXT = "3. Custom lock test";
  public static final String QUIT_TEXT = "4. Quit";

}
