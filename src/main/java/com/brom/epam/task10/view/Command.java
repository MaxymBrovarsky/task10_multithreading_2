package com.brom.epam.task10.view;

public interface Command {
  void execute();
}
