package com.brom.epam.task10.view;

import com.brom.epam.task10.controller.Controller;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class View {
  private Logger logger = LogManager.getLogger(View.class.getName());
  private Scanner input = new Scanner(System.in);
  private Map<String, String> menu;
  private Map<String, Command> menuCommands;
  private Controller controller;

  public View() {
    initMenu();
    initMenuCommands();
    controller = new Controller();
  }

  private void initMenu() {
    this.menu = new HashMap<>();
    this.menu.put(MenuConstants.MONITOR_KEY,
        MenuConstants.MONITOR_TEXT);
    this.menu.put(MenuConstants.PIPE_KEY,
        MenuConstants.PIPE_TEXT);
    this.menu.put(MenuConstants.CUSTOM_LOCK_KEY,
        MenuConstants.CUSTOM_LOCK_TEXT);
    this.menu.put(MenuConstants.QUIT_KEY,
        MenuConstants.QUIT_TEXT);
  }

  private void initMenuCommands() {
    this.menuCommands = new HashMap<>();
    this.menuCommands.put(MenuConstants.MONITOR_KEY, this::showMonitorExample);
    this.menuCommands.put(MenuConstants.PIPE_KEY, this::showPipedThreadsExample);
    this.menuCommands.put(MenuConstants.CUSTOM_LOCK_KEY, this::showOwnLockExample);
    this.menuCommands.put(MenuConstants.QUIT_KEY, this::quit);
  }

  private void showMonitorExample() {
    controller.executeLockExample();
  }

  private void showPipedThreadsExample() {
    controller.showPipedThread();
  }

  private void showOwnLockExample() {
    controller.testCustomLock();
  }

  private void quit() {
    System.exit(0);
  }

  public void show() {
    while (true) {
      menu.values().forEach(v -> logger.info(v));
      String command = input.nextLine();
      this.menuCommands.get(command).execute();
    }
  }
}
